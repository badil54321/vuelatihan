import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Component1 from '@/components/compo'
import SoalComponent from '@/components/soalUjian'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/compo',
      name: 'Component1',
      component: Component1
    },
    {
      path: '/soalUjian',
      name: 'SoalComponent',
      component: SoalComponent
    },
  ]
})
